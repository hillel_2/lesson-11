from flask import Flask

from src.config import FlaskConfig
from src.db import db
from src.routes import auth_route
from src.routes import user
from src.routes import base


app = Flask(__name__)
app.config.from_object(FlaskConfig)
db.init_app(app)
app.register_blueprint(auth_route)
app.register_blueprint(user.user_route)
app.register_blueprint(base.base_route)
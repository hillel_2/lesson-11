import hashlib

import requests

from src.models.user_models import UserModel


def hash_password(password: str) -> str:
    if password == '':
        raise ValueError('Password cannot be empty')
    if not isinstance(password, str):
        raise TypeError('')
    return hashlib.new('sha256', password.encode('utf-8')).hexdigest()


def generate_fake_user() -> UserModel:
    data = requests.get('https://randomuser.me/api/')
    random_user = data.json()
    person = random_user.get('results')[0]
    password = person['login']['password']
    password_hash = hash_password(password)
    print(password)
    return UserModel(**{
        'username': person['login']['username'],
        'password_hash': password_hash,
        'first_name': person['name']['first'],
        'last_name': person['name']['last'],
        'country': person['location']['country'],
        'email': person['email'],
        'dob': person['dob']['date']
    })

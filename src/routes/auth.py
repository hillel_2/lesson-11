from flask import render_template, request, redirect, Blueprint
from sqlalchemy.exc import MultipleResultsFound, NoResultFound

from src.db import db
from src.models.user_models import UserModel
from src.forms import LoginForm, SignupForm
from src.utils.helper import hash_password

auth_route = Blueprint('auth', __name__, url_prefix='/auth')

@auth_route.route('/login', methods=['GET', 'POST'])
def login():
    method = request.method
    form = LoginForm()
    if method == 'POST':
        if form.validate():
            username = form.username.data
            password = form.password.data
            password_hash = hash_password(password)
            try:
                user_model = db.session.query(
                    UserModel.id
                ).filter(
                    UserModel.username == username
                ).filter(
                    UserModel.password_hash == password_hash
                ).one()
            except MultipleResultsFound:
                return render_template('error.html', error='Знайдено більше одного користувача.')
            except NoResultFound:
                return render_template('error.html', error='Такого користувача не знайдено')
            return redirect(f'/user/{user_model.id}')
        return redirect(f'/login')
    else:
        return render_template('login.html', form=form)

@auth_route.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    method = request.method
    if method == 'POST':
        if form.validate():
            password = form.password.data
            password_hash = hash_password(password)
            new_user = UserModel(
                username=form.username.data,
                first_name=form.firstname.data,
                last_name=form.lastname.data,
                country=form.country.data,
                email=form.email.data,
                dob=form.dob.data,
                password_hash=password_hash
            )
            db.session.add(new_user)
            db.session.commit()
            return redirect(f'/user/{new_user.id}')
        return redirect(f'/signup')
    else:
        return render_template('signup.html', form=form)




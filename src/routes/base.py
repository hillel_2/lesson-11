from flask import Blueprint

base_route = Blueprint('Base', __name__, url_prefix='/')


@base_route.route('/')
def root():
    return {'message': 'Hello world'}

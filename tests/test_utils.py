from unittest.mock import patch, Mock

import pytest

from src.utils.helper import hash_password, generate_fake_user


def add(a, b):
    return a + b


def test_add():
    a = 1
    b = 2
    expected_result = 3
    actual_result = add(a, b)
    assert actual_result == expected_result


@pytest.mark.parametrize('password,expected_hash', [
    ('password', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
    ('12345', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5'),
    ('mixed123!@', 'a86bbd364db94d44c7e6dca0227f2bd0f6fe328cafd98eafe28c6cfcb0f26932'),
])
def test_hash_password__ok(password, expected_hash):
    actual_hash = hash_password(password)
    assert expected_hash == actual_hash


def test_hash_password__value_error():
    with pytest.raises(ValueError):
        hash_password('')


@pytest.mark.parametrize('bad_input', [
    None,
    123456,
    {},
    [],
    set()
])
def test_hash_password__bad_password(bad_input):
    with pytest.raises(TypeError):
        hash_password(bad_input)


def test_hash_password__good(fixture_password_with_hash):
    password = fixture_password_with_hash['password']
    expected_hash = fixture_password_with_hash['hash']
    actual_hash = hash_password(password)
    assert expected_hash == actual_hash


def test_generate_fake_user(fixture_password_with_hash):
    password = fixture_password_with_hash['password']
    expected_hash = fixture_password_with_hash['hash']
    fake_resp = {
        'results': [
            {
                "name": {
                    "first": "Test First",
                    "last": "Test Last"
                },
                "location": {
                    "country": "Ukraine",
                },
                "email": "testusername@example.com",
                "login": {
                    "username": "testusername",
                    "password": password,
                },
                "dob": {
                    "date": "1972-05-27T15:21:42.530Z"
                }
            }
        ]
    }
    with patch('requests.get', Mock(name="mock_get")) as request_get:

        mock_resp = Mock(name='mock_resp')
        mock_resp.json.return_value = fake_resp

        request_get.return_value = mock_resp
        test_user = generate_fake_user()
        assert test_user.username == 'testusername'
        assert test_user.password_hash == expected_hash
        assert test_user.first_name == 'Test First'
        assert test_user.last_name == 'Test Last'
        assert test_user.country == 'Ukraine'

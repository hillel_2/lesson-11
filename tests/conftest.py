from pytest import fixture

from src.app import app


@fixture
def fixture_password_with_hash():
    print('Fixture fixture_password_with_hash called')
    return {
        'password': 'password',
        'hash': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
    }


@fixture
def client():
    print('start tests')
    with app.test_client() as client:
        yield client
    print('end tests')
